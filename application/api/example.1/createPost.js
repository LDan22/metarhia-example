({
  access: 'public',
  method: async ({ title, text }) => {
    const createdPost = await db.pg.insert('Post', { title, text });
    return { result: 'success', data: createdPost };
  }
});
