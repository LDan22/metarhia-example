({
  Entity: {},

  title: { type: 'string', length: { min: 3, max: 255 } },
  text: { type: 'string' },
});
